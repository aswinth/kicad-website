+++
title = "Professional Support"
summary = "Business support options for KiCad"
aliases = [ "/professional-support/" ]
[menu.main]
    parent = "Help"
    name   = "Professional Support"
    weight = 50
+++
:toc: macro
:toc-title:

toc::[]

== Dedicated support service providers

Professional circuit designers can now receive dedicated, private support for KiCad from these providers:


{{< aboutlink "/img/about/ksc-logo.png" "https://www.kipro-pcb.com/" >}}

https://www.kipro-pcb.com/[KiCad Services Corporation] is a full-service commercial
support corporation, founded in 2019 with the mission of helping our professional users succeed and
thrive with KiCad.  We provide private issue reporting, rapid fixes and online remote desktop
support.  Additionally, we offer contracted feature development options.