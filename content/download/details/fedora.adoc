+++
title = "Install on Fedora"
summary = "Install instructions for KiCad on Fedora"
+++

== WARNING: This page is obsolete.
Please visit link:/download/linux[Install on Linux] for current information.

== Stable Release

{{< repology fedora_rawhide >}}

{{< repology fedora_39 >}}

{{< repology fedora_38 >}}

To install the most recent native KiCad package on Fedora, simply search for it
in the Software app or open a terminal and run the following command:

[source,bash]
dnf install kicad

NOTE: As of KiCad 5.0.0, the 3D models have been moved to a separate package and can
be installed with the following command:

[source,bash]
dnf install kicad-packages3d

Documentation is also in a separate package and can be installed with the
following command:

[source,bash]
dnf install kicad-doc

You can also install all three packages in a single command:

[source,bash]
dnf install kicad kicad-packages3d kicad-doc


In case the latest KiCad release is not yet available in the updates repository,
chances are you'll find it in the testing repository. Run the following command
to install a testing package:

[source,bash]
dnf --enablerepo=updates-testing install kicad

NOTE: The Fedora upgrade policy doesn't normally allow major version changes
in numbered Fedora releases.  However, please be aware that an exception to
that policy will be made in the case of critical security-related bugs.
This will be a rare event.

For those users who _do_ want to upgrade to a new major version of KiCad
in a numbered Fedora release, we've got you covered; simply choose one of the
following two options:

*Option 1:* Use the link:https://copr.fedorainfracloud.org/coprs/g/kicad/kicad/[Copr build service].
If you don't have Copr enabled yet, install and enable it with:

[source,bash]
dnf install dnf-plugins-core
dnf copr enable @kicad/kicad

Next, install or upgrade to KiCad 7 (including the 3D and documentation
packages as described above, if you like).  There are two command
variations; please choose the one that fits your situation:

*Variation 1:* If you had not previously installed KiCad 6, you can go right to
installing KiCad 7 from Copr by typing:

[source,bash]
dnf install kicad

*variation 2:* if you had previously installed KiCad 6, then you would instead _upgrade_
to KiCad 7:

[source,bash]
dnf upgrade kicad


*Option 2:* Use Flatpak.
The stable flatpak version of KiCad 7 can be installed as described
in link:{{% ref path="flatpak.adoc" %}}[KiCad Flatpak].

== Nightly Development Builds

The _nightly development builds_ are snapshots of the development codebase
(master branch) at a specific time. This codebase is under active development,
and while we try our best, it may contain more bugs than usual. New features added
to KiCad can be tested in these builds.

WARNING: Please read link:/help/nightlies-and-rcs/[Nightly Builds and Release Candidates] for
		 important information about the risks and drawbacks of using nightly builds.

TIP: Nightly builds are provided in a separate installation directory. It is
possible to install nightly builds at the same time as a stable version.

Nightly development builds for Fedora are available via the
link:https://copr.fedorainfracloud.org/coprs/g/kicad/kicad/[Copr build service],
and can be installed with:

[source,bash]
dnf install dnf-plugins-core
dnf copr enable @kicad/kicad
dnf install kicad-nightly

The kicad-nightly package includes the latest revision of all libraries except
for the 3D models from the
link:https://gitlab.com/kicad/libraries/kicad-packages3D[kicad-packages3D]
repository. Because of their size of more than 5 GB on disk they have been moved
to a separate package and can be installed with:

[source,bash]
dnf install kicad-nightly-packages3d
